++ Users ++

{
    _id : objectID,
    firstName : string,
    lastName : string,
    email : string,
    password : string,
    isAdmin : bool, 
    mobileNo : string, data does not need computing will be a string
    enrollment : [{
        ** Array of object
        courseID : string,
        courseName : string,
        enrolledOn: date
    }]
}



++ courses ++ 

{
    _id : ObjectID,
    name : string,
    description : string,
    price : number,
    isActive : boolean,
    slots : number,
    enrollees : [
        {
            **object
            userID : objectID,
            enrolledOn : Date
        }
    ]
    createdOn : date,
}


++ Transactions ++ 

{   
    _id = ObjectId,
    userID : objectId,
    courseID : string,
    totalAmount : integer
    isPaid : boolean, 
    paymentMethod : string;
    dateTimeCreated : date,
}

studentID : < 1 student to pay > 

Data Model Design
    the main consideration for the structure of your documents in your database 

two kinds of data model

== Embedded Data model ===========================

Company Data model (branches is embedded)
{
    "_id" : ObjecId,
    "name" : "abc company",
    "contact" : "091919092",
    "branches" : [
        {
            "name" : "branch1",
            "address" : "Quezon City",
            "contact" : "099978764",
        }
    ]
}

== Reference Data model =======================

    - 1 or more fields are being referenced from other data model

company datea model \\\\
{
      "_id" : ObjecId,
    "name" : "abc company",
    "contact" : "091919092",
    "branches" : [
        {
            "branchID" : ObejctID,
        }
    ]
}

branches data model \\\\
     {
            "_id" : ObjecId,  
            "companyID" : ObjectID, // the id coming from the company data model
            "name" : "branch1",
            "address" : "Quezon City",
            "contact" : "099978764",
    }


create another user that is an admin and make the enrollments array empty

Translating data models into an entity relationship diageram

it amis to show what attributes (fields) an entity (collection) has. It also shows the relationship of attributes
ERD are commonly used for SQL an relational databases

since NoSQL is a non relational database meaning there are no joining tables that connect multibple tables together, they would normally have
arrays with a list of unique ids of other objects that conenct them.

symbols that are currently used in erd diagrams
one
many

drawing an erd using diagrams.net

Activity:
1. In the S22 folder, create an activity folder.
2. Go to diagrams.net and link your professional gmail account.
3. Using the given set of entities and attributes, create an ERD that will show the relationships between the collections:
* Users (ATLEAST 2 - ADMIN AND NON-ADMIN)
    - First Name
    - Last Name
    - Email
    - Password
    - Is Admin?
    - Mobile Number
* Order
    - User Id
    - Transaction Date
    - Status
    - Total
* Products (ATLEAST 2 - ACTIVE AND INACTIVE PRODUCTS)
    - Name
    - Description
    - Price
    - Stocks
    - Is Active?
    - SKU
* Order Products
    - Order ID
    - Product ID
    - Quantity
    - Price
    - Sub Total
4. Download the diagram into the activity folder.
5. Create a mock data for each of the following collection using JSON syntax.
6. Create a git repository named S22.
7. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
8. Add the link in Boodle.


